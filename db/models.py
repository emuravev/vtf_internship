from django.db import models


class Hotel(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

class User(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    type = models.CharField(max_length=10)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} - {}'.format(self.first_name, self.last_name, self.hotel)

class RoomCategory(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    price = models.IntegerField()

    def __str__(self):
        return '{} - {}'.format(self.name, self.hotel)

class Room(models.Model):
    room_category = models.ForeignKey(RoomCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return '{} - {}'.format(self.name, self.room_category)

class Booking(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    date_check_in = models.DateTimeField()
    date_check_out = models.DateTimeField()

    def __str__(self):
        form = '%H:%M %m/%d'
        return 'from {} to {} - {}'.format(self.date_check_in.strftime(form), self.date_check_out.strftime(form), self.room)
# Create your models here.
