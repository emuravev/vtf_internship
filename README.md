# Done
+ list hotels => return list of hotels;
+ list RoomCategory => return list of RoomCategories;
+ list Rooms => return list of Rooms;
+ list Rooms of some RoomCategory => return list of Rooms of some RoomCategory if not specfied (`any` option choosen) return list of all Rooms;
+ list Bookings => return list of Bookings;
+ list Bookings of some Room => return list of Bookings of some Room if not specified (`any` option choosen) return list of all Bookings;
+ create Booking, care about overlaping (not full, only check correctness of input data);
+ get for Hotel, Room, Booking, RoomCategory => return serialized json object at `/hotel/get`;
+ list of users => return list of users from `User` table;

# Not done
- user login;
- user logout;
- user change password;
- REST API;