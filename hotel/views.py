from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.core import serializers

from db.models import Room, RoomCategory, Booking, Hotel

from datetime import datetime

def index(request):
    return render(request, 'hotel_index.html')

def room_category(request):
    rooms = RoomCategory.objects.all()

    return render(request, 'hotel_rooms_category.html', {'rooms' : rooms})

def __resolve_rooms(key):
    if key == 'any':
        return Room.objects.all()
    else:
        return Room.objects.filter(room_category = int(key)).all()

def rooms(request):
    categories = RoomCategory.objects.all()

    try:
        category = request.POST['rooms']
    except MultiValueDictKeyError:
        category = 'any'
    
    rooms = __resolve_rooms(category)

    return render(request, 'hotel_rooms.html', {'rooms' : rooms, 'categories' : categories})

def __resolve_bookings(key):
    if key == 'any':
        return Booking.objects.all()
    else:
        return Booking.objects.filter(room = int(key)).all()

def bookings(request):
    rooms = Room.objects.all()

    try:
        room = request.POST['rooms']
    except MultiValueDictKeyError:
        room = 'any'

    booking = __resolve_bookings(room)

    return render(request, 'hotel_booking.html', {'booking' : booking, 'rooms' : rooms})

def __create_book(cin, cout, room):
    f = '%Y-%m-%dT%H:%M'
    try:
        din = datetime.strptime(cin, f)
        dout = datetime.strptime(cout, f)
    except ValueError:
        return False
    if din > dout:
        return False
    
    rooms = Booking.objects.filter(room=room).all()
    i = Booking(room=Room.objects.filter(id=int(room)).all()[0], date_check_in=din, date_check_out=dout)
    i.save()
 #   for r in rooms:
 #       if r.date_check_in > din and r.date_check_out < dout:
 #           return False
 #       if r.date_check_in > din and r.date_check_out > dout:
 #           return False
 #       if r.date_check_in < din and r.date_check_out < dout:
 #           return False
 #       if r.date_check_in < din and r.date_check_out > dout:
 #           return False
    return True

def create_bookings(request):
    rooms = Room.objects.all()
    
    try:
        cin = request.POST['check_in_date']
        cout = request.POST['check_out_date']
        room = request.POST['room']

        created = __create_book(cin, cout, room)
        rooms = None 
    except MultiValueDictKeyError:
        created = None

    return render(request, 'create_booking.html', {'rooms' : rooms, 'created' : created})

def get_book(request, id):
    return HttpResponse(serializers.serialize('json', [get_object_or_404(Booking, id=id)]))

def get_room(request, id):
    return HttpResponse(serializers.serialize('json', [get_object_or_404(Room, id=id)]))

def get_hotel(request, id):
    return HttpResponse(serializers.serialize('json', [get_object_or_404(Hotel, id=id)]))

def get_category(request, id):
    return HttpResponse(serializers.serialize('json', [get_object_or_404(RoomCategory, id=id)]))
