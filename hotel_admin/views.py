from django.shortcuts import render

from db.models import User, Hotel

def index(request):
    return render(request, 'hotel_admin_index.html')

def list_users(request):
    users = User.objects.all()

    return render(request, 'hotel_admin_users.html', {'users' : users})

def list_hotels(request):
    hotels = Hotel.objects.all()

    return render(request, 'hotel_admin_hotels.html', {'hotels' : hotels})

