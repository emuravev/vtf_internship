from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('room_category', views.room_category, name='room_category'),
    path('rooms', views.rooms, name='rooms'),
    path('bookings', views.bookings, name='bookings'),
    path('create_bookings', views.create_bookings, name='create_bookings'),

    path('get/book/<int:id>', views.get_book, name='get_book'),
    path('get/room/<int:id>', views.get_room, name='get_room'),
    path('get/hotel/<int:id>', views.get_hotel, name='get_hotel'),
    path('get/category/<int:id>', views.get_category, name='get_category')
]
