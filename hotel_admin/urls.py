from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list_hotels', views.list_hotels, name='list_hotels'),
    path('list_users', views.list_users, name='list_users')
]
