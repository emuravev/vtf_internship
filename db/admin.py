from django.contrib import admin

from .models import User, Hotel, RoomCategory, Room, Booking

admin.site.register(User)
admin.site.register(Hotel)
admin.site.register(RoomCategory)
admin.site.register(Room)
admin.site.register(Booking)

# Register your models here.
